package com.example.ugquiz_springautomobiles.entities;


import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@EqualsAndHashCode
@Table(name = "AUTOMOBILES")
public class Automobile {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "automobileIdSeq", sequenceName = "AUTOMOBILE_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "automobileIdSeq")
    private Long id;

    @Column(name= "GOVERNMENT_NUMBER", nullable = false)
    private String governmentNumber;

    @Column(name= "MANUFACTURER", nullable = false)
    private String manufacturer;

    @Enumerated(value = EnumType.ORDINAL)
    @Column(name= "TYPE", nullable = false)
    private CarType type;

    @Column(name= "YEAR", nullable = false)
    private Integer year;

}
