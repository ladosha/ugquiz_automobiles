package com.example.ugquiz_springautomobiles.entities;

public enum CarType {
    SEDAN, HATCHBACK, UNIVERSAL
}
