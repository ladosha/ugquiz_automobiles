package com.example.ugquiz_springautomobiles.services;


import com.example.ugquiz_springautomobiles.entities.Automobile;
import com.example.ugquiz_springautomobiles.repositories.AutomobileRepository;
import com.example.ugquiz_springautomobiles.specifications.AutomobileSpecifications;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AutomobileService {


    private final AutomobileRepository automobileRepository;

    @Autowired
    public AutomobileService(AutomobileRepository automobileRepository) {
        this.automobileRepository = automobileRepository;
    }

    public Object addAutomobile(Automobile automobile) {
        if(automobile == null || automobile.getManufacturer() == null
                || automobile.getGovernmentNumber() == null || automobile.getYear() == null) {
            return new Exception("invalid arguments");
        }

        try {
            return this.automobileRepository.save(automobile);
        }
        catch (Exception e) {
            return e;
        }
    }

    public List<Automobile> searchAutomobiles(Automobile automobile) {
        if(automobile.getYear() == null || automobile.getManufacturer() == null) {
            return new ArrayList<Automobile>();
        }

        Specification<Automobile> specification = Specification
                .where(AutomobileSpecifications.manufacturerEquals(automobile.getManufacturer()))
                .and(AutomobileSpecifications.newerThan(automobile.getYear()));

        return this.automobileRepository.findAll(specification);

        //return this.automobileRepository.findAllByManufacturerAndYearOrderByYearDesc(automobile.getManufacturer(), automobile.getYear());
    }
}
