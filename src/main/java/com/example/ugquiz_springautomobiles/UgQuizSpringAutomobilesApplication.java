package com.example.ugquiz_springautomobiles;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UgQuizSpringAutomobilesApplication {

    public static void main(String[] args) {
        SpringApplication.run(UgQuizSpringAutomobilesApplication.class, args);
    }

}
