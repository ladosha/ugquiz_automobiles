package com.example.ugquiz_springautomobiles.controllers;

import com.example.ugquiz_springautomobiles.entities.Automobile;
import com.example.ugquiz_springautomobiles.services.AutomobileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AutomobileController {
    private final AutomobileService automobileService;

    @Autowired
    public AutomobileController(AutomobileService automobileService) {
        this.automobileService = automobileService;
    }

    @PostMapping("/automobile/add")
    public Object addAutomobile(@RequestBody Automobile automobile) {
        return automobileService.addAutomobile(automobile);
    }

    @PostMapping("/automobile/search")
    public List<Automobile> searchAutomobiles(@RequestBody Automobile automobile) {
        return automobileService.searchAutomobiles(automobile);
    }

}
