package com.example.ugquiz_springautomobiles.repositories;

import com.example.ugquiz_springautomobiles.entities.Automobile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface AutomobileRepository extends JpaRepository<Automobile, Long>, JpaSpecificationExecutor<Automobile> {

    List<Automobile> findAllByManufacturerAndYearOrderByYearDesc(String manufacturer, Integer year);


}
