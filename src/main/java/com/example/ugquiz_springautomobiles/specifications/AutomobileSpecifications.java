package com.example.ugquiz_springautomobiles.specifications;

import com.example.ugquiz_springautomobiles.entities.Automobile;
import org.springframework.data.jpa.domain.Specification;

public final class AutomobileSpecifications {

    private AutomobileSpecifications() {

    }

    public static Specification<Automobile> manufacturerEquals(String manufacturer) {
        return ((root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("manufacturer"), manufacturer));
    }

    public static Specification<Automobile> newerThan(Integer year) {
        return ((root, query, criteriaBuilder) -> criteriaBuilder.greaterThanOrEqualTo(root.get("year"), year));
    }


}
